package com.example.forgottenumbrella.cardboardmuseum

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar

/**
 * Settings activity for Cardboard Museum.
 */
class SettingsActivity : AppCompatActivity() {
    /**
     * Load [SettingsFragment] and otherwise set up UI.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Initialise settings on first launch.
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false)
        supportFragmentManager.commit(allowStateLoss = true) {
            replace(
                android.R.id.content,
                SettingsFragment()
            )
        }

        // Request notification permission if we need to and it has not yet been granted.
        if (
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS)
            != PackageManager.PERMISSION_GRANTED
        ) {
            val shouldShowPermissionRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.POST_NOTIFICATIONS
                )
            if (shouldShowPermissionRationale) {
                NotificationPermissionRequestDialogFragment.show(supportFragmentManager)
            } else {
                NotificationPermissionRequestDialogFragment.requestPermission(this)
            }
        }
    }

    /**
     * Load Preview button into [menu].
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    /**
     * Open Danbooru in the browser with the specified options so users know what sorts of wallpapers they'll get.
     */
    @Suppress("UNUSED_PARAMETER") // Required by menu_main.xml (OnClickListener.onClick)
    fun previewPosts(menuItem: MenuItem) {
        previewPosts()
    }

    /**
     * See [previewPosts].
     *
     * Internal for testing.
     */
    internal fun previewPosts(
        context: Context = this,
        settings: Settings = Settings.load(context),
    ) {
        val uri = ArtService.create(settings).previewUri(settings)
        val browserIntent = Intent(Intent.ACTION_VIEW, uri)
        if (browserIntent.resolveActivity(context.packageManager) != null) {
            context.startActivity(browserIntent)
        } else {
            Snackbar.make(
                context,
                findViewById(android.R.id.content),
                context.getString(R.string.error_no_browser_text),
                Snackbar.LENGTH_SHORT
            ).show()
            Log.w(LOG_TAG, "No browser for previews found")
        }
    }
}

/**
 * Settings fragment for [SettingsActivity].
 */
class SettingsFragment : PreferenceFragmentCompat(),
    SharedPreferences.OnSharedPreferenceChangeListener {
    /**
     * Load settings.
     */
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }

    /**
     * Refresh artworks when settings change.
     */
    override fun onSharedPreferenceChanged(prefs: SharedPreferences?, key: String?) {
        val context = context ?: return
        ArtProviderWorker.enqueueLoad(context, refresh = true)
    }

    /**
     * Register preference change listener on resume.
     *
     * This is recommended by the documentation for proper lifecycle management:
     * https://developer.android.com/guide/topics/ui/settings/use-saved-values#onsharedpreferencechangelistener
     */
    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences?.registerOnSharedPreferenceChangeListener(this)
    }

    /**
     * Unregister preference change listener on pause.
     */
    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences?.unregisterOnSharedPreferenceChangeListener(this)
    }
}

/**
 * Dialog fragment to explain why Cardboard Museum needs notification permission.
 */
class NotificationPermissionRequestDialogFragment : DialogFragment() {
    companion object {
        /**
         * Code to identify notification permission requests.
         */
        private const val REQUEST_CODE = 0

        /**
         * Tag to identify the notification permission explanation dialog fragment.
         */
        private const val TAG = "notification_permission_explanation"

        /**
         * Show the dialog.
         */
        fun show(fragmentManager: FragmentManager) {
            NotificationPermissionRequestDialogFragment()
                .show(fragmentManager, TAG)
        }

        /**
         * Request notification permission.
         */
        @RequiresApi(Build.VERSION_CODES.TIRAMISU)
        fun requestPermission(activity: Activity) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(Manifest.permission.POST_NOTIFICATIONS),
                REQUEST_CODE
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val activity = activity ?: throw IllegalStateException("Activity cannot be null")
        return AlertDialog.Builder(activity)
            .setTitle("Permission request")
            .setMessage("Cardboard Museum requires permission to notify about errors")
            .setPositiveButton("OK") { _, _ -> requestPermission(activity) }
            .create()
    }
}
