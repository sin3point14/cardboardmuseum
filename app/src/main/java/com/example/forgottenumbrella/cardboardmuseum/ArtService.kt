package com.example.forgottenumbrella.cardboardmuseum

import android.net.Uri
import android.util.Log
import com.google.android.apps.muzei.api.provider.Artwork

/**
 * A service that fetches artworks from some source.
 */
internal interface ArtService {
    /**
     * Return a list of artworks matching the given [settings].
     */
    fun getArtworks(settings: Settings): List<Artwork>

    /**
     * Return a URL for previewing available artworks.
     */
    fun previewUri(settings: Settings): Uri

    companion object {
        /**
         * Return the user-selected art service.
         */
        fun create(settings: Settings): ArtService {
            return when (settings.imageboard) {
                "Danbooru" -> DanbooruService()
                "Gelbooru" -> GelbooruService()
                else -> {
                    Log.w(
                        LOG_TAG,
                        "Got unexpected imageboard: ${settings.imageboard}, defaulting to Danbooru"
                    )
                    DanbooruService()
                }
            }
        }
    }
}
