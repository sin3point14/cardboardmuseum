@file:Suppress("SpellCheckingInspection")

package com.example.forgottenumbrella.cardboardmuseum

import android.content.ComponentName
import android.content.Intent
import android.content.IntentFilter
import androidx.core.net.toUri
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.spyk
import io.mockk.verify
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.Shadows

/**
 * Unit tests for the [SettingsActivity].
 */
@RunWith(AndroidJUnit4::class)
class SettingsActivityTest {
    private val context = spyk(ApplicationProvider.getApplicationContext()).apply {
        every { startActivity(any()) } just Runs
    }

    @Test
    fun previewPosts_opensCorrectUrl() {
        // Given...
        val settings = Settings(
            tags = "touhou",
            safeMode = true,
            random = true,
            username = "AzureDiamond",
            key = "hunter2",
        )
        with(Shadows.shadowOf(context.packageManager)) {
            val component = ComponentName("com.example", "ExampleBrowser")
            addActivityIfNotPresent(component)
            addIntentFilterForActivity(
                component,
                IntentFilter(Intent.ACTION_VIEW).apply {
                    addCategory(Intent.CATEGORY_DEFAULT)
                    addDataScheme("https")
                    addDataAuthority("danbooru.donmai.us", null)
                }
            )
        }
        // When...
        Robolectric.buildActivity(SettingsActivity::class.java).create().get()
            .previewPosts(context, settings)
        // Then...
        val expected = (
                "https://danbooru.donmai.us/posts?tags=touhou rating:general"
                        + "&random=true&login=AzureDiamond&api_key=hunter2"
                ).toUri()
        verify { context.startActivity(match { it.data == expected }) }
    }

    // TODO: Assert it explains notification permission and requests it on startup if not yet granted
}
