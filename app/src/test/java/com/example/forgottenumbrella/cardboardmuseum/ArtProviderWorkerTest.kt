@file:Suppress("SpellCheckingInspection")

package com.example.forgottenumbrella.cardboardmuseum

import android.content.Context
import android.net.Uri
import androidx.core.net.toUri
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.android.apps.muzei.api.provider.Artwork
import com.google.android.apps.muzei.api.provider.ProviderClient
import io.mockk.Called
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.verify
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Call
import retrofit2.Response
import retrofit2.mock.Calls

/**
 * Unit tests for [ArtProviderWorker].
 *
 * TODO: Gelbooru tests
 */
@RunWith(AndroidJUnit4::class) // Required for interacting with context.
class ArtProviderWorkerTest {
    companion object {
        /**
         * Example of a safe and valid post.
         */
        private val VALID_POST = DanbooruRetrofitService.Post(
            id = "3293594",
            characters = "tatara_kogasa",
            copyrights = "touhou",
            artists = "nnyara",
            source = "https://twitter.com/nnyara_N/status/1053961002734088192",
            fileUrl = "https://danbooru.donmai.us/data/9d2bc7cade59b1ae1dfaad98f1ba5a5c.jpg",
            tags = "1girl :d blue_hair blush commentary_request geta juliet_sleeves long_sleeves looking_at_viewer nnyara one_eye_closed open_mouth puffy_sleeves rain red_eyes short_hair smile solo tatara_kogasa touhou twitter_username umbrella",
            rating = "s",
        )

        /**
         * Example of an NSFW post.
         */
        private val NSFW_POST = DanbooruRetrofitService.Post(
            id = "4997947",
            characters = "gawr_gura",
            copyrights = "hololive hololive_english",
            artists = "riito_uwu",
            source = "https://i.pximg.net/img-original/img/2021/07/28/09/01/52/91551332_p0.png",
            fileUrl = "https://cdn.donmai.us/original/bc/dc/bcdc3048a03e0987c13d0e0a741550ad.png",
            tags = "1girl absurdres bangs bdsm blue_eyes blue_hair blush bondage bound breasts gawr_gura highres hololive hololive_english multicolored_hair naked_ribbon restrained ribbon ribbon_bondage riito_uwu silver_hair small_breasts streaked_hair virtual_youtuber",
            rating = "q",
        )

        /**
         * Magic string for API failures.
         */
        private const val BAD_APPLE = "FAIL"

        /**
         * Magic string for irrelevant values.
         */
        private const val MIMA = "IRRELEVANT"
    }

    private val context = ApplicationProvider.getApplicationContext<Context>()
    private val mockProvider = mockk<ProviderClient>().apply {
        every { addArtwork(any<List<Artwork>>()) } returns listOf()
        every { setArtwork(any<Artwork>()) } returns Uri.EMPTY
    }

    @Test
    fun doWork_notifiesAuthFailure() {
        // Given...
        val settings = Settings(context, username = BAD_APPLE, key = BAD_APPLE)
        val mockRetrofit = mockk<DanbooruRetrofitService>().apply {
            every {
                listPosts(
                    tags = any(),
                    random = any(),
                    username = BAD_APPLE,
                    key = BAD_APPLE
                )
            } returns run {
                val rawResponse = """{"success":false,"reason":"authentication failed"}"""
                val errorBody = rawResponse.toResponseBody("application/json".toMediaType())
                val errorCode = 401
                Calls.response(Response.error(errorCode, errorBody))
            }
        }
        val mockService = DanbooruService(mockRetrofit)
        mockNotifyError {
            // When...
            doWork(context, mockProvider, settings, mockService)
            // Then...
            verify {
                notifyError(
                    context,
                    context.getString(R.string.notification_error_authentication_text)
                )
                mockProvider wasNot Called
            }
        }
    }

    @Test
    fun doWork_notifiesTagLimitWithoutRandom() {
        // Given...
        val settings = Settings(context, tags = "1 2 3", random = false)
        val mockRetrofit = mockk<DanbooruRetrofitService>().apply {
            every {
                listPosts(
                    tags = match { tagCount(it) > 2 },
                    random = false,
                    username = any(),
                    key = any()
                )
            } returns tagLimitResponse()
        }
        val mockService = DanbooruService(mockRetrofit)
        mockNotifyError {
            // When...
            doWork(context, mockProvider, settings, mockService)
            // Then...
            verify {
                notifyError(context, context.getString(R.string.notification_error_limit_text))
                mockProvider wasNot Called
            }
        }
    }

    @Test
    fun doWork_notifiesTagLimitWithRandom() {
        // Given...
        val settings = Settings(context, tags = "1 2", random = true)
        val mockRetrofit = mockk<DanbooruRetrofitService>().apply {
            every {
                listPosts(
                    tags = match { tagCount(it) > 1 },
                    random = true,
                    username = any(),
                    key = any()
                )
            } returns tagLimitResponse()
        }
        val mockService = DanbooruService(mockRetrofit)
        mockNotifyError {
            // When...
            doWork(context, mockProvider, settings, mockService)
            // Then...
            verify {
                notifyError(context, context.getString(R.string.notification_error_limit_text))
                mockProvider wasNot Called
            }
        }
    }

    @Test
    fun doWork_notifiesEmpty() {
        // Given...
        val mockRetrofit = mockk<DanbooruRetrofitService>().apply {
            every { listPosts(any(), any(), any(), any()) } returns Calls.response(listOf())
        }
        val mockService = DanbooruService(mockRetrofit)
        mockNotifyError {
            // When...
            doWork(context, mockProvider, artService = mockService)
            // Then...
            verify {
                notifyError(context, context.getString(R.string.notification_error_empty_text))
                mockProvider wasNot Called
            }
        }
    }

    @Test
    fun doWork_respectsSafeMode() {
        // Given...
        val settings = Settings(context, safeMode = true)
        val mockRetrofit = mockk<DanbooruRetrofitService>().apply {
            every {
                listPosts(
                    tags = match { it.contains(Regex("""\brating:s""")) },
                    random = any(),
                    username = any(),
                    key = any(),
                )
            } returns Calls.response(listOf(VALID_POST))
            every {
                listPosts(
                    tags = match { !it.contains(Regex("""\brating:s""")) },
                    random = any(),
                    username = any(),
                    key = any(),
                )
            } returns Calls.response(listOf(VALID_POST, NSFW_POST))
        }
        val mockService = DanbooruService(mockRetrofit)
        // When...
        ArtProviderWorker.doWork(context, mockProvider, settings, mockService)
        // Then...
        verify {
            mockProvider.addArtwork(match<List<Artwork>> { artworks ->
                artworks.all { artwork ->
                    // If the artwork has no metadata (i.e. metadata is null), then it's an error.
                    DanbooruRetrofitService.Post.Metadata.fromJson(artwork.metadata!!)?.rating == "s"
                }
            })
        }
    }

    @Test
    fun doWork_refreshes() {
        // Given...
        val mockRetrofit = mockk<DanbooruRetrofitService>().apply {
            every {
                listPosts(
                    any(),
                    any(),
                    any(),
                    any()
                )
            } returns Calls.response(listOf(VALID_POST, NSFW_POST))
        }
        val mockService = DanbooruService(mockRetrofit)
        // When...
        ArtProviderWorker.doWork(context, mockProvider, artService = mockService, refresh = true)
        // Then...
        verify {
            // Cannot test against exact artworks, because Artwork does not implement value-based equality.
            mockProvider.setArtwork(any<Artwork>())
            mockProvider.addArtwork(any<List<Artwork>>())
        }
    }

    @Test
    fun doWork_artworksValid() {
        // Given...
        val mockRetrofit = mockk<DanbooruRetrofitService>().apply {
            every {
                listPosts(
                    any(),
                    any(),
                    any(),
                    any()
                )
            } returns Calls.response(listOf(VALID_POST))
        }
        val mockService = DanbooruService(mockRetrofit)
        mockNotifyError {
            // When...
            doWork(context, mockProvider, artService = mockService)
            // Then...
            verify {
                mockProvider.addArtwork(
                    match<List<Artwork>> {
                        val artwork = it.single()
                        artwork.token == "3293594"
                                && artwork.title == "tatara kogasa (touhou)"
                                && artwork.byline == "nnyara"
                                && artwork.attribution == "https://twitter.com/nnyara_N/status/1053961002734088192"
                                // If the persistent URI is null, then it's an error.
                                && artwork.persistentUri ==
                                "https://danbooru.donmai.us/data/9d2bc7cade59b1ae1dfaad98f1ba5a5c.jpg".toUri()
                                && artwork.webUri == "https://danbooru.donmai.us/posts/3293594".toUri()
                                && artwork.metadata ==
                                DanbooruRetrofitService.Post.Metadata(
                                    tags = "1girl :d blue_hair blush commentary_request geta juliet_sleeves long_sleeves looking_at_viewer nnyara one_eye_closed open_mouth puffy_sleeves rain red_eyes short_hair smile solo tatara_kogasa touhou twitter_username umbrella",
                                    rating = "s",
                                ).toJson()
                    }
                )
            }
            verify(inverse = true) { notifyError(any(), any()) }
        }
    }

    @Test
    fun doWork_titlesCorrect() {
        // Given...
        val characters = listOf(
            "", // 0
            "tatara_kogasa", // 1
            "cirno daiyousei", // 2
            "luna_child star_sapphire sunny_milk", // 3
            "flandre_scarlet hong_meiling izayoi_sakuya patchouli_knowledge remilia_scarlet", // 5
            "fujiwara_no_mokou houraisan_kaguya inaba_tewi kamishirasawa_keine reisen_udongein_inaba yagokoro_eirin", // 6
        )
        val posts = characters.map {
            DanbooruRetrofitService.Post(
                id = MIMA,
                characters = it,
                copyrights = MIMA,
                artists = MIMA,
                source = MIMA,
                fileUrl = MIMA,
                tags = MIMA,
                rating = MIMA,
            )
        } + NSFW_POST
        val mockRetrofit = mockk<DanbooruRetrofitService>().apply {
            every {
                listPosts(any(), any(), any(), any())
            } returns Calls.response(posts)
        }
        val mockService = DanbooruService(mockRetrofit)
        // When...
        ArtProviderWorker.doWork(context, mockProvider, artService = mockService)
        // Then...
        verify {
            mockProvider.addArtwork(
                match<List<Artwork>> {
                    it[0].title == MIMA
                            && it[1].title == "tatara kogasa ($MIMA)"
                            && it[2].title == "cirno and daiyousei ($MIMA)"
                            && it[3].title == "luna child, star sapphire, and sunny milk ($MIMA)"
                            && it[4].title == "flandre scarlet, hong meiling, izayoi sakuya, patchouli knowledge, and remilia scarlet ($MIMA)"
                            && it[5].title == "fujiwara no mokou, houraisan kaguya, inaba tewi, kamishirasawa keine, reisen udongein inaba, and 1 more ($MIMA)"
                            && it[6].title == "gawr gura (hololive and 1 more)"
                }
            )
        }
    }

    /**
     * Run a block with the [ArtProviderWorker.notifyError] function mocked out.
     */
    private fun mockNotifyError(block: ArtProviderWorker.Companion.() -> Unit) {
        mockkObject(ArtProviderWorker) {
            every { ArtProviderWorker.notifyError(any(), any()) } just Runs
            block.invoke(ArtProviderWorker.Companion)
        }
    }

    /**
     * Return the number of tags that count towards the limit by Danbooru.
     */
    private fun tagCount(tags: String) =
        tags.split(" ").filter(String::isNotEmpty).filterNot { it.startsWith("rating:") }.size

    /**
     * Return response for when tag limit is reached.
     */
    private fun tagLimitResponse(): Call<List<DanbooruRetrofitService.Post>> {
        val rawResponse =
            """{"success":false,"message":"You cannot search for more than 2 tags at a time"}"""
        val errorBody = rawResponse.toResponseBody("application/json".toMediaType())
        val errorCode = 500
        return Calls.response(Response.error(errorCode, errorBody))
    }
}
