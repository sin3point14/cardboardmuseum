plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("com.google.devtools.ksp")
}

android {
    namespace = "com.example.forgottenumbrella.cardboardmuseum"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.example.forgottenumbrella.cardboardmuseum"
        minSdk = 19
        targetSdk = 34
        versionCode = 17
        versionName = "2.1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJunitRunner"

        // Needed because apk is too big and targets min SDK < 20.
        multiDexEnabled = true
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    kotlin {
        jvmToolchain(17)
    }

    // Needed for Robolectric.
    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }
}

dependencies {
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.fragment:fragment-ktx:1.6.2")
    // Needed due to apk size.
    implementation("androidx.multidex:multidex:2.0.1")
    implementation("androidx.preference:preference-ktx:1.2.1")
    implementation("androidx.work:work-runtime-ktx:2.9.0")
    implementation("com.google.android.apps.muzei:muzei-api:3.4.1")
    implementation("com.squareup.moshi:moshi:1.15.0")
    implementation(platform("com.squareup.okhttp3:okhttp-bom:4.11.0"))
    implementation("com.squareup.okhttp3:okhttp")
    implementation("com.squareup.okhttp3:logging-interceptor")
    implementation("com.squareup.retrofit2:converter-moshi:2.9.0")
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.google.android.material:material:1.11.0")
    ksp("com.squareup.moshi:moshi-kotlin-codegen:1.15.0")
    testImplementation("androidx.test:core:1.5.0")
    testImplementation("com.squareup.retrofit2:retrofit-mock:2.9.0")
    testImplementation("io.mockk:mockk:1.13.5")
    testImplementation("org.robolectric:robolectric:4.10.3")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
}
