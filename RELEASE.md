# Steps to create a release
1. Bump version name and version code in `app/build.gradle.kts`
2. Update `CHANGELOG.md`
3. ~~Verify tests pass~~

   Tests are currently broken due to broken test libraries:
   - https://github.com/robolectric/robolectric/issues/8165

   Fixing this would require converting the unit tests into instrumented tests:
   - https://github.com/google/conscrypt/issues/649

   Help appreciated!
4. Build release APK
5. Create tag: `git tag -a vX.Y.Z`
6. Push to upstream: `git push --follow-tags`
7. Create release
