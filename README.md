# Cardboard Museum
A [Muzei][] 3.0 plugin for the (potentially NSFW) ~~[Danbooru][]~~ [Gelbooru][] imageboard that
gently refreshes your home screen with anime-style pictures matching given tags.

**Note:** This plugin previously supported Danbooru,
however Danbooru has since restricted programmatic access due to AI scrapers.
Cardboard Museum has thus transitioned to using Gelbooru.

## Installation
To build and install the app, you can download the source code and use [Android Studio][], or run
```sh
./gradlew installDebug
```
under Java 9+ (only [OpenJDK][] 11, 14 and 17 have been tested however).

Alternatively, you can download and install the app from [Releases][] or [F-Droid][].

Be sure to enable Unknown Sources in Settings if it's blocked.

## License
The source code is available under [GPL-3.0-or-later][].

The Cardboard Museum icon is available under [CC-BY-SA-4.0][], created by Leo Pham.

[Muzei]: https://github.com/romannurik/muzei
[Danbooru]: https://danbooru.donmai.us
[Gelbooru]: https://gelbooru.com
[Android Studio]: https://developer.android.com/studio
[OpenJDK]: https://openjdk.java.net
[Releases]: https://gitlab.com/ForgottenUmbrella/cardboardmuseum/-/releases
[F-Droid]: https://f-droid.org/en/packages/com.example.forgottenumbrella.cardboardmuseum
[GPL-3.0-or-later]: https://www.gnu.org/licenses/gpl-3.0.html
[CC-BY-SA-4.0]: https://creativecommons.org/licenses/by-sa/4.0/legalcode
